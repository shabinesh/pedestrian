Django==1.6.1
dj-database-url==0.2.2
psycopg2==2.5.1
wsgiref==0.1.2
gunicorn==18.0
south
