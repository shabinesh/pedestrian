from django.conf.urls import patterns, include, url
from places.models import Place

from django.contrib import admin
admin.autodiscover()

from places.views import *

urlpatterns = patterns('',
    # Examples:
    url(r'^$', PlacesList.as_view(template_name='index.html'), name='home'),
    url(r'^maps/$', MapView.as_view(), name='maps'),
    url(r'^places/$', PlacesList.as_view(), name='places'),
    url(r'^p/', include('django.contrib.flatpages.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^post/', include('posts.urls', namespace='posts')),
)
