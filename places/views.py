from django.shortcuts import render
from django.views.generic.list import ListView

from .models import *
from posts.models import *
# Create your views here.

class PlacesList(ListView):
    queryset = Place.objects.all()
    context_object_name = 'places'
    
    def get_context_data(self, *args, **kwargs):
        p = Post.objects.all().order_by('-created')[:5]
        context = super(PlacesList, self).get_context_data(*args, **kwargs)
        context.update({'posts': p})
        return context

class MapView(PlacesList):
    template_name = 'places/maps.html'
