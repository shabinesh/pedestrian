from django.db import models
from django_countries.fields import CountryField
# Create your models here.

class Place(models.Model):
    lat = models.FloatField()
    lng = models.FloatField()
    name = models.CharField(max_length=200)
    country = CountryField()
    wikilink = models.URLField()

    def __unicode__(self):
        return unicode(self.name)
