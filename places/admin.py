from django.contrib import admin

# Register your models here.
from .models import *

class PlaceAdmin(admin.ModelAdmin):
    pass


admin.site.register(Place, PlaceAdmin)
