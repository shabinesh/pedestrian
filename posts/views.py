from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from .models import *
from places.models import * 

# Create your views here.

class PostDetailView(DetailView):
    model = Post
    context_object_name = 'post'

    def get_context_data(self, *args, **kwargs):
        context = super(self.__class__, self).get_context_data(*args, **kwargs)
        return context

class PostList(ListView):
    model = Post
    context_object_name = 'post'

    def get_context_data(self, *args, **kwargs):
        context = super(PostList, self).get_context_data(*args, **kwargs)
        context['countries'] = [
