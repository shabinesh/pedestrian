from django.db import models

from places.models import Place
# Create your models here.

class Affiliate(models.Model):
    title = models.CharField(max_length=40)
    link = models.TextField()

    def __unicode__(self):
        return unicode(self.title)

class Post(models.Model):
    title = models.CharField(max_length=200)
    title_pic = models.ImageField(upload_to='%Y/%m/title')
    thumbnail_pic = models.ImageField(upload_to='%Y/%m/thumbnails/')
    place = models.ForeignKey(Place)
    content = models.TextField()
    promotions = models.ManyToManyField(Affiliate, null=True)
    created = models.DateTimeField(auto_now=True)
    slug = models.SlugField()
    published = models.BooleanField(default = False)
    
    def __unicode__(self):
        return unicode(self.title)

class Image(models.Model):
    place = models.ForeignKey(Place, null=True)
    post = models.ForeignKey(Post, null = True, related_name="images")
    title = models.CharField(max_length=200)
    image = models.ImageField()
