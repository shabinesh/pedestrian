from django.conf.urls import patterns, include, url
from django.views.generic.list import ListView
from django.views.generic import TemplateView
from .views import *
from .models import *

urlpatterns = patterns('',
        url(r'^(?P<slug>[\w-]+)/$', PostDetailView.as_view(), name='post'),
        #url(r'^edit/new/$', TemplateView.as_view(template_name="posts/post_new.html"), name="new_post"),
        url(r'^$', ListView.as_view(
            model=Post, 
            queryset=Post.objects.all(), 
            paginate_by=10,
            context_object_name='posts'), name='all-posts')
)
